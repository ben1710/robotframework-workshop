# Robot Framework Workshop

## Excercises

### Basics 1
**Prerequisites:**
* Robot Framework installed
  
      pip install robotframework


**In this excercise you will learn:**
* basic libraries **BuiltIn** and **String**
* Robot Framework data types (string, number, boolean)
* using lists and dictionaries (**Collections** library)
* handling errors (**OperatingSystem** library)
* logging

**Excercise:**
1. Open Basics_1_Examples.robot
2. Inspect and run examples
   1. inspect defined variables and different data types
   2. run test suite
   
            robot test-suites/01__RF_Basics/Basics_1_Examples.robot
   
3. Log the data types of the existing variables using *Log To Console*
   1. evaluate keyword (**BuiltIn** library) and 
   2. type(\${variable}) or type(\${variable}).\_\_name\_\_
   
4. Working with lists
   1. create a second list @{list_2} using the keyword Create List with a different value at position 6
   2. write a test case that compares the lists @{list} and @{list_2} using the **Collections** library
   3. start the test suite with parameter -d logs
   
            robot -d logs test-suites/01__RF_Basics/Basics_1_Examples.robot

    1. inspect logs/report.html
   
5. Working with dictionaries
   1. Verify that the mountain_elevation dictionary does not contain 'Watzmann'
   2. Create a second dictionary ${city_population} with the entries Berlin(~3.8 million) and Hamburg(~1.9)
   3. Create a list ${german_facts} containing the two dictionaries and log the population of hamburg using the created list



### Basics 2
**Prerequisites:**
* --

**In this excercise you will learn:**
* interacting with files (**OperatingSystem** library)
* handling content of files (**String** library)
* using a for loop

**Excercise:**
1. Open Basics_2_Examples.robot
2. inspect and run examples
   1. inspect defined test case and execute test suite
   2. write a test case to check whether file exists and the content is correct
   3. write a test case that splits the file content into entries and create a table-like structure. The result should look like
   
        ``` [['Mueller', '42', 'Hamburg'], ['Meier', '67', 'Muenchen'], ['Wuensch', '38', 'Koeln']] ```
   
   *hint: use the **String** library and for loop*



### Basics 3
**Prerequisites:**
* --

**In this excercise you will learn:**
* how to use **Telnet** library
* how to ignore a buggy test case (commandline options *--exclude* or *--noncritical*)

**Excercise:**
1. Open Basics_3_Examples.robot
2. Inspect and run examples
3. Retrieve the forecast for New York City (code:NYC)
   * copy the example and adapt the code
   
    *hint: Try your commands from local commandline. If it is not available yet you need to activate it (on windows; admin rights necessary!):*

                dism /online /Enable-Feature /FeatureName:TelnetClient

4. telnet to cimt-ag
   1. connect to cimt-ag.de, port 23 via telnet
   2. run test suite and inspect report.html
   3. ignore failed test case: 
   *hint: use commandline options:*
    
        http://robotframework.org/robotframework/2.8.6/RobotFrameworkUserGuide.html#all-command-line-options

   1. run test suite and inspect report.html
