*** Settings ***
Documentation  Example of how to extend Robot Framework by your own python keyword library
...            and generating documentation for libraries. 
Library  CustomPython.py
Library  String


*** Test Cases ***
Example
  ${answer}=  Return Answer    arg
  Log  ${answer}

Exercise 1a
  ${result}=  join two strings  hello  world
  Should be equal  ${result}  hello|world

Exercise 1b
  ${res}=  Count Number Of Words  ${RESOURCES}${/}sample.txt
  Should Be Equal As Integers  ${res}  10