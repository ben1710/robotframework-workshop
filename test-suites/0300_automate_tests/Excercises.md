# Robot Framework Workshop

## Excercises

### Automate test suites 

#### Create a Gitlab pipeline
**Prerequisites:**
* --

**In this excercise you will learn:**
* how to use Gitlab Continuous Integration
* how to do a dryrun of your test suites
* how to create a yml file to automate your test suites

**Excercise:**  
Gitlab does not only provide git support but is a complete software development platform including Continuous Integration.

  Reference: [Quickstart](https://gitlab.com/help/ci/quick_start/README)  
  Reference: [CI/CD in Gitlab](https://docs.gitlab.com/ee/ci/README.html)  
  Reference: [Pipeline manual for Gitlab](https://docs.gitlab.com/ee/ci/yaml/README.html)  
  Reference: [Pipeline architectures for Gitlab](https://gitlab.com/help/ci/pipelines/pipeline_architectures.md)  

1. Create a `.gitlab-ci.yml` file in the main folder of the repository with the following content:

```yml
stages:
  - build
  - run
  
Build my job:
  stage: build
  script:
    - sleep 3
    - echo This is my build step

Run test-suites folder:
  stage: run
  script:
    - sleep 3
    - echo This is my run step
  only:
    - master
```

2. Check your projects **CI/CD** section for running pipelines. What stages had been executed?
3. In your projects **Merge Request** section create a merge request for this branch on to your *master*
4. Execute merge when pipeline succeeds

5. In order to have an environment ready for your test suites add the following lines to your yml file after the stages section:
```yml

default:
  image: python:3.7-slim-buster
  before_script:
    - python -m pip install --upgrade pip setuptools wheel
    - pip install -r install/requirements.txt --upgrade

```

6. Add another stage 'test' to your yml file and a dry-run of your test suites (only test suites without dependencies)
```yml
Dry Run test-suites folder:
  stage: test
  script:
    - robot --argumentfile run/args.txt --dryrun test-suites/0100__RF_Basics
```

7. Add another stage 'run' to your yml file and run your test suites (only test suites without dependencies)
```yml

Run test-suites folder:
  stage: run
  script:
    - robot --argumentfile run/args.txt test-suites/0100__RF_Basics
  artifacts:
    paths:
      - $CI_PROJECT_DIR/logs/*
    expire_in: 2 hours
    when: always
  only:
    - master
```