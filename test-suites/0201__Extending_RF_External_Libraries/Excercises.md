# Robot Framework Workshop

## Excercises

### External Libraries 1
**Prerequisites:**
* Install the external library **SeleniumLibrary**
   
        pip install robotframework-seleniumlibrary

* Get a suitable webdriver for your browser: 
   
    ``` https://www.selenium.dev/downloads/ ```

  * chrome 
  
      ``` https://chromedriver.chromium.org/downloads, https://sites. google.com/a/chromium.org/chromedriver/ ```
  * internet explorer 
  
      ``` https://github.com/SeleniumHQ/selenium/wiki/InternetExplorerDriver#required-configuration ```
  * firefox 
  
     ``` https://firefox-source-docs.mozilla.org/testing/geckodriver/Support.html ```
  
* Put the driver into a folder that is in PATH variable


**In this excercise you will learn:**
* different test styles (data driven, keyword driven, behaviour driven)
* to use the **Seleniumlibrary** and locators
* writing custom robot keywords with arguments
* managing libraries using requirements.txt
* difference between test and suite setup/teardown

**Excercise:**  
1. Open the file *install/requirements.txt* and add the just installed libraries as dependency

        robotframework-seleniumlibrary==<version>
        
2. Open External_Libraries_1_Examples.robot
3. Inspect and run examples
4. Write a test case that successfully performs a login at ${url} with valid credentials (admin:admin)
5. Write a test case that expects that login should fail using several invalid logins (empty username, empty password, wrong user, wrong password, ...)
   * The test case itself should not fail!
   * move all steps to a separate keyword in a separate file (resources/keywords/custom_selenium.robot) and use that file


    *hint: If you are not familiar with locators/locating web elements you can use the following*

     * "Sign In" element:  //*[@id="header"]/div[2]/div/div/nav/div[1]/a
     * "Login Email" element: //*[@id="email"]
     * "Login Password" element: //*[@id="passwd"]
     * "Submit Login" element: //*[@id="SubmitLogin"]/span
     * "Authentication succesful" element: //*[@id="columns"]/div[1]/span[2]
     * "Authentication failed" element: //*[@id="authentication"]

    *hint: you can use the following keywords*
      * *Click Element*, *Wait Until Page Contains Element*, *Input Text*, *Input Password*, *Page Should Contain Element*, *Open Browser*, *Close Browser*


### External Libraries 2
**Prerequisites:**
* postgres database started (localhost, user=postgres, password=postgres, port=5432, database=robot)
* Install **Database** library and psycopg2(postgres driver)

      pip install robotframework-databaselibrary
      pip install psycopg2
* Talend Job pg_robot (https://cimtag.sharepoint.com/:f:/s/KTDWHBI/ErDK1zq8QKFHmsmtDqY6Em8BJXg33iTLA2tKwsj5YBrcDw?e=a45lhM)

**In this excercise you will learn:**
* using **Database** library with postgres database
* using *Suite Setup* declaration
* writing keywords with arguments
* using variable files (commandline option *--variablefile*)
* ETL example: prepare database, run talend job, verify result

**Excercise:**
1. Open the file *install/requirements.txt* and add the just installed libraries as dependency

        robotframework-databaselibrary==<version>
        psycopg2==<version>

2. Open External_Libraries_2_Examples.robot
3. Inspect and run examples
4. Write an idempotent keyword *Prepare Database* 
   * which is executed at the beginning of all tests
   * the keyword is supposed to create the table *workshop.mytest* (columns: name varchar, surname varchar, state varchar)
5. Write a test case which inserts one row into the table, queries the table and verifies that the result is correct
6. Outsource the variables for the database connection into a file within the *config* folder. Replace all hardcoded strings with those variables. The variables should be replaces automatically when executing the test suite.

    *hint:*
      * use commandline option *--variablefile config/var_postgres.py* (run/args.txt)

7. Verify that all variables are replaced by using *Log Variables* keyword
   
8. Copy the Talend job *pg_robot* to a local directory of your choice. The job shall be executed with the parameter ``` --context_param pg_tablename=mytest ```
9. Write another test case that verifies that the job wrote 12 rows and two of them are:
     * Tom Sawyer from Wyoming
     * Mark Twain from California



*Library documentation:*

  ``` http://franz-see.github.io/Robotframework-Database-Library/api/1.2.2/DatabaseLibrary.html ``` 


  ```http://franz-see.github.io/Robotframework-Database-Library/```
